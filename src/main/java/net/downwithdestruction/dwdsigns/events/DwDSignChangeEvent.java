package net.downwithdestruction.dwdsigns.events;

import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.block.SignChangeEvent;

public class DwDSignChangeEvent extends SignChangeEvent {
	public DwDSignChangeEvent(final Block theBlock, final Player thePlayer, final String[] theLines) {
		super(theBlock, thePlayer, theLines);
	}
}
