package net.downwithdestruction.dwdsigns.utils;

import net.downwithdestruction.dwdsigns.DwDSigns;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class SignEditor {
	private final Sign editor, updated;
	private final Block updatedSignBlk, editorBlk;
	
	public SignEditor(final Block block, final Block blockUpdatedSign) {
		editorBlk = block;
		updatedSignBlk = blockUpdatedSign;
		updated = (Sign) updatedSignBlk.getState();
		editor = (Sign) editorBlk.getState();
		final String[] lines = updated.getLines();
		for (int i=0;i<4;i++) editor.setLine(i, lines[i]);
		editor.update();
	}
	
	public void updateSign(final String[] lines) {
		for (int i=0;i<4;i++) updated.setLine(i, DwDSigns.colorize(lines[i]));
		updated.update();
	}
	
	public Sign getEditor() {
		return editor;
	}
	
	public Sign getUpdated() {
		return updated;
	}
	
	public Block getUpdatedSignBlk() {
		return updatedSignBlk;
	}
	
	public void removeEditorSign(final Player p) {
		editorBlk.setType(Material.AIR);
		p.getInventory().setItemInHand(new ItemStack(Material.SIGN, 1));
	}
	
	public Block getEditorBlk() {
		return editorBlk;
	}
}
