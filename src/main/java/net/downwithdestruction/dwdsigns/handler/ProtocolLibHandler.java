package net.downwithdestruction.dwdsigns.handler;

import org.bukkit.ChatColor;
import org.bukkit.Location;

import net.downwithdestruction.dwdsigns.DwDSigns;
import net.downwithdestruction.dwdsigns.utils.SignEditor;

import com.comphenix.protocol.Packets;
import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.events.ConnectionSide;
import com.comphenix.protocol.events.PacketAdapter;
import com.comphenix.protocol.events.PacketContainer;
import com.comphenix.protocol.events.PacketEvent;
import com.comphenix.protocol.reflect.FieldAccessException;

public class ProtocolLibHandler implements PacketHandler {
	private DwDSigns plugin;

	public ProtocolLibHandler(DwDSigns plugin) {
		this.plugin = plugin;
		ProtocolLibrary.getProtocolManager().getAsynchronousManager().registerAsyncHandler(new PacketAdapter(plugin, ConnectionSide.CLIENT_SIDE, Packets.Client.UPDATE_SIGN) {
			@Override
			public void onPacketReceiving(final PacketEvent event) {
				try {
					PacketContainer packet = event.getPacket();
					int x = packet.getIntegers().read(0);
					int y = packet.getIntegers().read(1);
					int z = packet.getIntegers().read(2);
					String[] lines = packet.getStringArrays().read(0);
					final SignEditor saved = DwDSigns.signEditor.get(new Location(event.getPlayer().getWorld(), x, y, z));
					if (saved != null) saved.updateSign(lines);
				} catch (FieldAccessException e) {
					((DwDSigns) plugin).log(ChatColor.RED + "[SEVERE] Couldn't access packet field: " + e);
				}
			}
		}).syncStart();
	}
	
	@Override
	public void shutdown() {
		ProtocolLibrary.getProtocolManager().removePacketListeners(plugin);
	}
}
