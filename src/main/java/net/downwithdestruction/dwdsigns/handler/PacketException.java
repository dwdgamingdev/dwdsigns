package net.downwithdestruction.dwdsigns.handler;

public class PacketException extends RuntimeException {
	private static final long serialVersionUID = 1L;
	
	public PacketException(String message) {
		super(message);
	}
	
	public PacketException(String message, Throwable throwable) {
		super(message, throwable);
	}
}
