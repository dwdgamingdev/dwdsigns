package net.downwithdestruction.dwdsigns.listeners;

import net.downwithdestruction.dwdsigns.DwDSigns;
import net.downwithdestruction.dwdsigns.events.DwDSignChangeEvent;
import net.downwithdestruction.dwdsigns.utils.SignEditor;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.block.SignChangeEvent;

import com.griefcraft.model.Protection;

public class SignListener implements Listener {
	private DwDSigns plugin;
	
	public SignListener(final DwDSigns plugin) {
		this.plugin = plugin;
	}
	
	@EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = true)
	public void onSignChange(final SignChangeEvent event) {
		if (event.isCancelled()) return;
		if (plugin.debug) plugin.log(ChatColor.YELLOW + "Sign change event triggered.");
		final Player player = event.getPlayer();
		if (plugin.hasLWC) {
			final Protection protection = plugin.lwc.findProtection(event.getBlock());
			if (protection != null) {
				if (!plugin.lwc.canAccessProtection(player, protection) ||
						!plugin.lwc.canAdminProtection(player, protection)) {
					if (plugin.debug) plugin.log(ChatColor.YELLOW + "The player, " + player + ", tried to edit a sign, but was denied.");
					player.sendMessage(ChatColor.RED + "You do not own this sign!");
					return;
				}
			}
		}
		if (event.getPlayer().hasPermission("dwdsigns.color")) {
			if (plugin.debug) plugin.log(ChatColor.YELLOW + "Color allowed!");
			for (int i=0;i<4;i++) event.setLine(i, DwDSigns.colorize(event.getLine(i)));
		}
	}
	
	@EventHandler(priority = EventPriority.LOW, ignoreCancelled = true)
	public void onSignEditorChange(final SignChangeEvent event) {
		if (event instanceof DwDSignChangeEvent) {
			if (plugin.debug) plugin.log(ChatColor.YELLOW + "Editor Sign change event triggered.");
			return;
		}
		final Location signLoc = event.getBlock().getLocation();
		final SignEditor saved = DwDSigns.signEditor.get(signLoc);
		if (saved == null) return;
		final Player player = event.getPlayer();
		if (plugin.hasLWC) {
			final Protection protection = plugin.lwc.findProtection(event.getBlock());
			if (protection != null) {
				if (!plugin.lwc.canAccessProtection(player, protection) ||
						!plugin.lwc.canAdminProtection(player, protection)) {
					if (plugin.debug) plugin.log(ChatColor.YELLOW + "The player, " + player + ", tried to edit a sign, but was denied.");
					player.sendMessage(ChatColor.RED + "You do not own this sign!");
					return;
				}
			}
		}
		final DwDSignChangeEvent signChange = new DwDSignChangeEvent(saved.getUpdatedSignBlk(), player, event.getLines());
		Bukkit.getPluginManager().callEvent(signChange);
		if (signChange.isCancelled()) {
			DwDSigns.signEditor.remove(signLoc);
			Bukkit.getPluginManager().callEvent(new BlockBreakEvent(event.getBlock(), player));
			return;
		}
		saved.updateSign(signChange.getLines());
		saved.removeEditorSign(player);
		Bukkit.getPluginManager().callEvent(new BlockBreakEvent(event.getBlock(), player));
		DwDSigns.signEditor.remove(signLoc);
		event.setCancelled(true);
	}
	
	@EventHandler(priority = EventPriority.LOWEST)
	public void signPlace(final BlockPlaceEvent event) {
		if (event.isCancelled()) return;
		final Block block = event.getBlockPlaced();
		if (block.getType() != Material.WALL_SIGN && block.getType() != Material.SIGN_POST) return;
		if (plugin.debug) plugin.log(ChatColor.YELLOW + "Sign place event triggered.");
		final Block blockAgainst = event.getBlockAgainst();
		if (!(blockAgainst.getState() instanceof Sign)) return;
		if (event.getPlayer().hasPermission("dwdsigns.click.signedit")) {
			if (!DwDSigns.signMode.get(event.getPlayer())) return;
			if (plugin.debug) plugin.log(ChatColor.YELLOW + "Opening sign editor.");
			DwDSigns.signEditor.put(block.getLocation(), new SignEditor(block, blockAgainst));
			if (plugin.hasProtoLib) event.setCancelled(true);
		}
	}
}
