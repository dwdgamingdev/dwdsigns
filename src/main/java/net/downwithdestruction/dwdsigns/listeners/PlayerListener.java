package net.downwithdestruction.dwdsigns.listeners;

import net.downwithdestruction.dwdsigns.DwDSigns;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;
import org.bukkit.event.Event.Result;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;

import com.griefcraft.model.Protection;

public class PlayerListener implements Listener {
	private DwDSigns plugin;
	
	public PlayerListener(final DwDSigns plugin) {
		this.plugin = plugin;
	}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onPlayerInteract(final PlayerInteractEvent event) {
		if(!event.hasBlock()) return;
		final Block block = event.getClickedBlock();
		final BlockFace face = event.getBlockFace();
		final Player player = event.getPlayer();
		if (event.getAction() == Action.RIGHT_CLICK_BLOCK) {
			if (block.getTypeId() == Material.CHEST.getId()) {
				if (event.hasItem()) {
					if ((face != BlockFace.UP) && (face != BlockFace.DOWN)) {
						if (event.getItem().getTypeId() == Material.SIGN.getId()) {
							if (block.getRelative(face).getTypeId() == Material.AIR.getId()) {
								if (plugin.hasLWC) {
									final Protection protection = plugin.lwc.findProtection(block);
									if (protection != null) {
										if (!plugin.lwc.canAccessProtection(player, protection) ||
												!plugin.lwc.canAdminProtection(player, protection)) {
											if (plugin.debug) plugin.log(ChatColor.YELLOW + "The player, " + player + ", tried to place a sign on chest, but was denied.");
											player.sendMessage(ChatColor.RED + "You do not own this chest!");
											return;
										}
									}
								}
								if (plugin.debug) plugin.log(ChatColor.YELLOW + "Placing sign on chest.");
								event.setUseItemInHand(Result.ALLOW);
								event.setUseInteractedBlock(Result.DENY);
							}
						}
					}
				}
			}
		} else if (event.getAction() == Action.LEFT_CLICK_BLOCK) {
			if (!plugin.hasLWC) return;
			if (block.getType() != Material.WALL_SIGN && block.getType() != Material.SIGN_POST) return;
			((Sign) block.getState()).update();
		}
	}
	
	@EventHandler
	public void onPlayerJoin(final PlayerJoinEvent event) {
		DwDSigns.signMode.put(event.getPlayer(), false);
	}
}
