package net.downwithdestruction.dwdsigns.commands;

import net.downwithdestruction.dwdsigns.DwDSigns;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CmdSignEdit implements CommandExecutor {
	private DwDSigns plugin;
	
	public CmdSignEdit(final DwDSigns plugin) {
		this.plugin = plugin;
	}
	
	private String getFinalArg(final String[] args, final int start) {
		 final StringBuilder bldr = new StringBuilder();
		 for (int i = start; i < args.length; i++) {
			 if (i != start) {
				 bldr.append(" ");
			 }
			 bldr.append(args[i]);
		 }
		 return bldr.toString();
	}
	
	@Override
	public boolean onCommand(final CommandSender cs, final Command cmd, final String label, final String[] args) {
		if (cmd.getName().equalsIgnoreCase("signedit")) {
			if (!(cs instanceof Player)) {
				cs.sendMessage(ChatColor.RED + "This command is only available to players!");
				return true;
			}
			if (!cs.hasPermission("dwdsigns.command.signedit")) {
				cs.sendMessage(ChatColor.RED + "You don't have permission for that!");
				plugin.log(ChatColor.GOLD + cs.getName() + ChatColor.RED + " was denied access to that!");
				return true;
			}
			if (args.length < 2) {
				cs.sendMessage(cmd.getDescription());
				return false;
			}
			Integer line;
			try {
				line = Integer.parseInt(args[0]);
			} catch(NumberFormatException e) {
				cmd.getDescription();
				cs.sendMessage(ChatColor.RED + "Line must be a number!");
				return false;
			}
			if ((line < 1) || (line > 4)) {
				cmd.getDescription();
				cs.sendMessage(ChatColor.RED + "Acceptable line range is 1 - 4 only!");
				return false;
			}
			final Block block = ((Player) cs).getTargetBlock(null, 10);
			if ((block.getType() != Material.WALL_SIGN) && (block.getType() != Material.SIGN_POST)) {
				cs.sendMessage(ChatColor.RED + "No sign targetted!");
				return true;
			}
			if (plugin.hasLWC) {
				if (plugin.lwc.findProtection(block) != null) {
					if (!plugin.lwc.canAccessProtection((Player) cs, block)) {
						cs.sendMessage(ChatColor.RED + "This sign is not owned by you!");
						return true;
					}
					if (!plugin.lwc.canAdminProtection((Player) cs, block)) {
						cs.sendMessage(ChatColor.RED + "You are not the admin of this sign!");
						return true;
					}
				}
			}
			final String newline = DwDSigns.colorize(getFinalArg(args,1));
			final Sign sign = (Sign) block.getState();
			sign.setLine((line - 1), newline);
			sign.update();
			if (plugin.debug) {
				plugin.log(ChatColor.GOLD + cs.getName() + ChatColor.YELLOW + " edited sign line # " + ChatColor.GOLD + (line+1) + ChatColor.YELLOW + " to '" + ChatColor.GOLD + newline + ChatColor.YELLOW + "'.");
			}
			cs.sendMessage(ChatColor.LIGHT_PURPLE + "Sign updated.");
			return true;
		}
		return false;
	}
}
