package net.downwithdestruction.dwdsigns.commands;

import net.downwithdestruction.dwdsigns.DwDSigns;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CmdSignMode implements CommandExecutor {
	private DwDSigns plugin;
	
	public CmdSignMode(final DwDSigns plugin) {
		this.plugin = plugin;
	}
	
	@Override
	public boolean onCommand(final CommandSender cs, final Command cmd, final String label, final String[] args) {
		if (cmd.getName().equalsIgnoreCase("signmode")) {
			if (!(cs instanceof Player)) {
				cs.sendMessage(ChatColor.RED + "This command is only available to players!");
				return true;
			}
			if (!cs.hasPermission("dwdsigns.command.signmode")) {
				cs.sendMessage(ChatColor.RED + "You don't have permission for that!");
				plugin.log(ChatColor.GOLD + cs.getName() + ChatColor.RED + " was denied access to that!");
				return true;
			}
			if (args.length < 1) {
				cs.sendMessage(cmd.getDescription());
				return false;
			}
			Boolean editmode;
			if (args[0].equalsIgnoreCase("edit")) editmode = true;
			else if (args[0].equalsIgnoreCase("build")) editmode = false;
			else {
				cs.sendMessage(ChatColor.RED + "Unrecognized mode. Use either " + ChatColor.YELLOW + "edit" + ChatColor.RED + " or " + ChatColor.YELLOW + "build" + ChatColor.RED + ".");
				cs.sendMessage(cmd.getDescription());
				return true;
			}
			if (!cs.hasPermission("dwdsigns.click.signedit")) {
				cs.sendMessage(ChatColor.LIGHT_PURPLE + "You do not have permission to edit signs by clicking.");
				editmode = false;
			}
			DwDSigns.signMode.put((Player) cs, editmode);
			if (plugin.debug) {
				plugin.log(ChatColor.GOLD + cs.getName() + ChatColor.YELLOW + " set signmode to '" + ChatColor.GOLD + (editmode ? "edit" : "build" ) + ChatColor.YELLOW + "'.");
			}
			cs.sendMessage(ChatColor.LIGHT_PURPLE + "Sign mode set to " + ChatColor.GRAY + (editmode ? "edit" : "build" ) + ChatColor.LIGHT_PURPLE + ".");
			return true;
		}
		return false;
	}
}
