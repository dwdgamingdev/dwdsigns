package net.downwithdestruction.dwdsigns;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.logging.Level;

import com.griefcraft.lwc.LWC;
import com.griefcraft.lwc.LWCPlugin;

import net.downwithdestruction.dwdsigns.utils.Metrics;
import net.downwithdestruction.dwdsigns.utils.SignEditor;
import net.downwithdestruction.dwdsigns.commands.*;
import net.downwithdestruction.dwdsigns.handler.PacketHandler;
import net.downwithdestruction.dwdsigns.handler.ProtocolLibHandler;
import net.downwithdestruction.dwdsigns.listeners.*;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

public class DwDSigns extends JavaPlugin {
	private final PluginManager pluginManager = Bukkit.getPluginManager();
	private PacketHandler handler;
	
	public final static HashMap<Location, SignEditor> signEditor = new HashMap<Location, SignEditor>();
	public final static HashMap<Player,Boolean> signMode = new HashMap<Player,Boolean>();
	public Boolean hasLWC = false;
	public Boolean hasProtoLib = false;
	public Boolean debug = false;
	public Boolean colorLogs = true;
	public LWC lwc;
	
	public void onEnable() {
		loadConfig();
		
		if (pluginManager.isPluginEnabled("LWC")) {
			log(ChatColor.AQUA + "Detected LWC, enabling protections!");
			hasLWC = true;
			lwc = ((LWCPlugin) pluginManager.getPlugin("LWC")).getLWC();
		};
		if (pluginManager.isPluginEnabled("ProtocolLib")) {
			log(ChatColor.AQUA + "Detected ProtocolLib, using that for handling!");
			hasProtoLib = true;
			handler = new ProtocolLibHandler(this);
		}
		
		pluginManager.registerEvents(new SignListener(this), this);
		pluginManager.registerEvents(new PlayerListener(this), this);
		
		getCommand("signedit").setExecutor(new CmdSignEdit(this));
		getCommand("signmode").setExecutor(new CmdSignMode(this));
		
		try {
			Metrics metrics = new Metrics(this);
			metrics.start();
		} catch (IOException e) { log(ChatColor.RED + "Failed to start Metrics: " + ChatColor.YELLOW + e.getMessage()); }
		
		log(ChatColor.YELLOW + "v" + this.getDescription().getVersion() + " by BillyGalbreath enabled!");
	}
	
	public void onDisable() {
		if (hasProtoLib) handler.shutdown();
		log(ChatColor.YELLOW + "Plugin Disabled.");
	}
	
	private void loadConfig() {
		final File file = new File(getDataFolder() + File.separator + "config.yml");
		try {
			if (!file.exists()) {
				file.getParentFile().mkdirs();
				file.createNewFile();
				final FileConfiguration config = YamlConfiguration.loadConfiguration(file);
				config.addDefault("debug-mode", "false");
				config.addDefault("color-logs", "true");
				config.options().copyDefaults(true);
				config.save(file);
				log(ChatColor.GOLD + "Created new default configuration file: " + file.getAbsolutePath());
			}
		} catch (IOException e) {
			log("Config file at " + file.getAbsolutePath() + " failed to load: " + e.getMessage());
		}
		debug = getConfig().getBoolean("debug-mode", false);
		colorLogs = getConfig().getBoolean("color-logs", true);
	}
	
	public void log (final Object obj) {
		if (colorLogs) {
			getServer().getConsoleSender().sendMessage(
					ChatColor.AQUA + "[" + ChatColor.LIGHT_PURPLE + getName() + ChatColor.AQUA + "] " + ChatColor.RESET + obj
			);
		} else {
			Bukkit.getLogger().log(Level.INFO, "[" + getName() + "] " + ((String) obj).replaceAll("(?)\u00a7([a-f0-9k-or])", ""));
		}
	}
	
	public static String colorize(final String str) {
		return str.replaceAll("(?i)&([a-f0-9k-or])", "\u00a7$1");
	}
}
